const testIt = require('./test-it')

let kebabFunc = testIt.getFunctionFrom("./kebab.js")

testIt.assert(
    "sam cmd, first item is salad", 
    kebabFunc({name:"sam"})[0] == "salade"
)

testIt.end()